(function($) {

	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	// function setWebcookie($kind) {
	// 	$.cookie('webcookie',$kind,{expires:7,path:'/'});
	// }
	// $('.setwebcookie').on('click',function(e){
	// 	var kind = $(this).attr('data-web');
	// 	setWebcookie(kind);
	// });
	$( document ).ready(function() {
		$('.version-site').click(function () {
			var typeSite = $(this).data('web');
			localStorage.setItem('version', typeSite);
			var versionSite = localStorage.getItem('version');
			$('.header-version').addClass(versionSite);
		});
		$('#inputArea').change(function() {
			var area = '';
			$( '#inputArea option:selected').each(function() {
				area = $( this ).data('id');
			});
			var select = $('.form-group').find($('#inputCity'));
			$('#inputCity').children('option:not(:first)').remove();
			jQuery.ajax({
				url: '/wp-admin/admin-ajax.php',
				type: 'post',
				dataType: 'json',
				data: {
					area: area,
					action: 'models_search',
				},
				success: function (data) {

					var options = data.html;
					$.each(options, function (i, item) {
						select.append($('<option>', {
							value: i,
							text : item
						}));
					});
				}
			});
		});
		$('#inputCity').change(
			function(){
				$(this).closest('form').trigger('submit');
			});
		$('.sidebar-item-trigger').click(function () {
			$(this).toggleClass('rotated');
			$(this).parent('.sidebar-item-inside').parent('.sidebar-item').addClass('active-side');
			$(this).parent('.sidebar-item-inside').parent('.sidebar-item').children('.children-terms-wrap').slideFadeToggle();
		});
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$('.trigger-shop-item').click(function (){
			$(this).toggleClass('active');
			$(this).parent('.trigger-shop-col').parent('.marker-item').children('.marker-addition').slideFadeToggle();
		});
		$('.search-trigger').click(function () {
			$('.pop-search').addClass('show-search');
			$('.float-search').addClass('show-float-search');
		});
		$('.close-search').click(function () {
			$('.pop-search').removeClass('show-search');
			$('.float-search').removeClass('show-float-search');
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.posts-slider').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1600,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 650,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$('.gallery-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			rtl: true,
			asNavFor: '.thumbs'
		});
		$('.thumbs').slick({
			slidesToShow: 6,
			slidesToScroll: 1,
			rtl: true,
			asNavFor: '.gallery-slider',
			dots: false,
			arrows: true,
			responsive: [
				{
					breakpoint: 1600,
					settings: {
						slidesToShow: 5,
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		});
		$('.product-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			rtl: true,
			asNavFor: '.product-thumbs'
		});
		$('.product-thumbs').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			asNavFor: '.product-slider',
			dots: false,
			arrows: true,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				// {
				// 	breakpoint: 768,
				// 	settings: {
				// 		slidesToShow: 2,
				// 	}
				// },
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		$('.play-button-post').click(function() {
			var id = $(this).data('id');
			var iFrame = '<iframe src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('.put-video-here').addClass('show').html(iFrame);
		});
		var accordionProduct = $('#accordion-product');
		accordionProduct.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.accordion-button-wrap').children('button').children('.minus-icon').removeClass('show-icon');
			collapsed.parent().children('.accordion-button-wrap').children('button').children('.plus-icon').removeClass('hide-icon');
		});
		accordionProduct.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.accordion-button-wrap').children('button').children('.minus-icon').addClass('show-icon');
			show.parent().children('.accordion-button-wrap').children('button').children('.plus-icon').addClass('hide-icon');
		});
		//Socials
		$('.trigger-wrap').hover(function(){
			$(this).children('.all-socials').addClass('show-share');
		}, function(){
			$(this).children('.all-socials').removeClass('show-share');
		});
	});
	$('.load-more-posts').click(function(e) {
		e.preventDefault();
		var btn = $(this);
		btn.addClass('loading');
		btn.append('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');
		var postType = $(this).data('type');
		var termID = $(this).data('term');
		var termName = $(this).data('term_name');
		// var params = $('.take-json').html();
		var ids = '';
		var page = $(this).data('page');
		var countAll = $(this).data('count');
		var quantity = $('.more-card').length;
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				postType: postType,
				termID: termID,
				termName: termName,
				ids: ids,
				page: page,
				quantity: quantity,
				countAll: countAll,
				// taxType: taxType,
				action: 'get_more_function',
			},
			success: function (data) {
				btn.removeClass('loading');
				$('.cart-loading').remove();
				if (!data.html || data.quantity) {
					btn.addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
})( jQuery );
