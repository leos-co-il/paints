<?php
/*
Template Name: דף הבית
*/

the_post();
get_header('home');
?>

<section class="home-main-block" <?php if (has_post_thumbnail()) : ?>
	style="background-image: url('<?= postThumb(); ?>')"
<?php endif; ?>>
	<div class="home-main-overlay"></div>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-9 col-md-10 col-12 d-flex flex-column justify-content-start align-items-center">
				<?php if ($logo_white = opt('logo_white')) : ?>
					<a href="<?= $logo_white['url']; ?>" class="logo logo-home">
						<img src="<?= $logo_white['url']; ?>" alt="logo">
					</a>
				<?php endif; ?>
				<div class="base-output main-page-output">
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-xl-8 col-md-9 col-12 d-flex flex-column justify-content-start align-items-center">
				<?php if ($links = opt('home_links')) : ?>
					<div class="row justify-content-around align-items-start">
						<?php foreach ($links as $link_current) : ?>
							<div class="col-auto col-main-margin">
								<h3 class="home-title">
									<?= $link_current['h_page_title']; ?>
								</h3>
								<a href="<?= ($link_current['h_link'] && isset($link_current['h_link']['url'])) ? $link_current['h_link']['url']
										: ''; ?>" class="base-link base-link-main version-site" data-web="<?= ($link_current['h_page_title'] == 'Professional') ? 'pro' : 'home'; ?>">
									<?= ($link_current['h_link'] && isset($link_current['h_link']['title'])) ? $link_current['h_link']['title']
											: 'למוצרים'; ?>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer('home'); ?>
