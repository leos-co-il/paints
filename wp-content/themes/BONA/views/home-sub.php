<?php
/*
Template Name: דף הבית השני
*/

get_header();
$fields = get_fields();

?>

<section class="home-main-block home-sub-block" <?php if (has_post_thumbnail()) : ?>
	style="background-image: url('<?= postThumb(); ?>')"
<?php endif; ?>>
	<div class="main-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php if ($fields['main_title']) : ?>
					<h1 class="homepage-main-title">
						<?= $fields['main_title']; ?>
					</h1>
				<?php endif; ?>
			</div>
		</div>
		<?php if ($fields['main_link']) : ?>
			<div class="row justify-content-center">
				<div class="col-auto mt-5">
					<a href="<?= isset($fields['main_link']['url']) ? $fields['main_link']['url'] : ''; ?>" class="base-link base-link-white">
						<?= isset($fields['main_link']['title']) ? $fields['main_link']['title'] : 'מעבר למוצרים'; ?>
					</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>
<?php if ($fields['h_cats'] || $fields['h_cats_text']) : ?>
	<section class="home-cats-block">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<?php if ($fields['h_cats_text']) : ?>
					<div class="col-xl-6 col-lg-8 col-12">
						<div class="base-output text-center">
							<?= $fields['h_cats_text']; ?>
						</div>
					</div>
				<?php endif;
				if ($fields['h_cats']) : ?>
				<div class="col-xl-9 col-lg-10 col-12">
					<div class="row justify-content-center align-items-start">
						<?php foreach ($fields['h_cats'] as $y => $cat) : ?>
							<div class="col-lg-4 col-6  wow fadeIn" data-wow-delay="0.<?= $y + 2; ?>s">
								<a href="<?= get_term_link($cat); ?>" class="category-item">
									<span class="cat-image">
										<?php if ($cat_img = get_field('cat_img', $cat)) : ?>
											<img src="<?= $cat_img['url']; ?>" alt="category-of-products">
										<?php endif; ?>
									</span>
									<span class="category-item-title">
										<?= $cat->name; ?>
									</span>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<?php if ($fields['h_cats_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= isset($fields['h_cats_link']['url']) ? $fields['h_cats_link']['url'] : ''; ?>" class="base-link base-link-white">
							<?= isset($fields['h_cats_link']['title']) ? $fields['h_cats_link']['title'] : 'מעבר למוצרים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['h_about_text'] || $fields['h_about_img']) : ?>
	<section class="about-block">
		<div class="about-block-container">
			<div class="about-block-item">
				<div class="about-text-wrapper">
					<div class="base-output slider-output">
						<?= $fields['h_about_text']; ?>
					</div>
					<?php if ($fields['h_about_link']) : ?>
						<a href="<?= isset($fields['h_about_link']['url']) ? $fields['h_about_link']['url'] : ''; ?>" class="base-link base-link-white">
							<?= isset($fields['h_about_link']['title']) ? $fields['h_about_link']['title'] : 'קרא עוד עלינו'; ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="about-block-item">
				<div class="about-block-img" style="background-image: url('<?= $fields['h_about_img']['url']; ?>')"></div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_about_benefits'] || $fields['h_about_ben_img']) : ?>
<section class="about-block">
	<div class="about-block-container">
		<div class="about-block-item">
			<div class="about-block-img" style="background-image: url('<?= $fields['h_about_ben_img']['url']; ?>')"></div>
		</div>
		<div class="about-block-item">
			<div class="about-benefits-wrapper">
				<div class="row justify-content-around align-items-start">
					<?php foreach ($fields['h_about_benefits'] as $benefit) : ?>
						<div class="col-sm-6 benefit-col">
							<div class="benefit-item">
								<div class="benefit-icon">
									<?php if ($benefit['ben_icon']) : ?>
										<img src="<?= $benefit['ben_icon']['url']; ?>">
									<?php endif; ?>
								</div>
								<h4 class="benefit-title">
									<?= $benefit['ben_title']; ?>
								</h4>
								<p class="base-text">
									<?= $benefit['ben_desc']; ?>
								</p>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;
if ($fields['h_projects']) : ?>
	<section class="home-posts">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="base-title">
						<?= $fields['h_projects_title']; ?>
					</h2>
				</div>
			</div>
			<?php if ($terms = $fields['h_projects_links']) {
				get_template_part('views/partials/repeat', 'links',
						[
								'links' => $terms,
						]);
			} ?>
			<div class="row align-items-stretch put-here-posts justify-content-center put-here-posts">
				<?php foreach ($fields['h_posts'] as $post) {
					get_template_part('views/partials/card', 'post_col',
							[
									'post' => $post,
							]);
				} ?>
			</div>
			<?php if ($fields['h_all_projects_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= isset($fields['h_all_projects_link']['url']) ? $fields['h_all_projects_link']['url'] : ''; ?>" class="base-link w-auto">
							<?= isset($fields['h_all_projects_link']['title']) ? $fields['h_all_projects_link']['title'] : 'לכל הפרוייקטים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['h_posts']) : ?>
	<section class="home-posts">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="base-title">
						<?= $fields['h_posts_title']; ?>
					</h2>
				</div>
			</div>
			<div class="row align-items-stretch put-here-posts justify-content-center put-here-posts">
				<?php foreach ($fields['h_posts'] as $post) {
					get_template_part('views/partials/card', 'post_col',
							[
									'post' => $post,
							]);
				} ?>
			</div>
			<?php if ($fields['h_posts_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= isset($fields['h_posts_link']['url']) ? $fields['h_posts_link']['url'] : ''; ?>" class="base-link">
							<?= isset($fields['h_posts_link']['title']) ? $fields['h_posts_link']['title'] : 'לכל המאמרים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
get_footer(); ?>
