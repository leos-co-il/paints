<?php
/*
Template Name: פוסטים
*/

get_header();
$fields = get_fields();
$post_type = $fields['type'] ? $fields['type'] : 'post';
$cat_type = 'project' ? 'project_cat' : 'category';
$posts = new WP_Query([
	'posts_per_page' => 8,
	'post_type' => $post_type,
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => $post_type,
]);
$terms = get_terms([
		'taxonomy'      => $cat_type,
		'hide_empty'    => false,
		'parent' => 0,
]);
?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="block-title">
					<?php the_title(); ?>
				</h1>
			</div>
		</div>
		<?php if ($terms) {
			get_template_part('views/partials/repeat', 'links',
					[
							'links' => $terms,
					]);
		} ?>
		<div class="row">
			<div class="col-12">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center put-here-posts">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post_col',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 8)) : ?>
			<div class="row justify-content-center mt-4">
				<div class="col-auto">
					<div class="more-link base-link load-more-posts" data-type="<?= $post_type; ?>" data-count="<?= $num; ?>">
						טען עוד..
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>

