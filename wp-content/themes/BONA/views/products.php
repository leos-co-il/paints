<?php
/*
Template Name: מוצרים
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => 6,
	'post_type' => 'product',
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'product',
]);
?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container-fluid">
		<div class="row justify-content-end">
			<div class="col-lg-9 col-md-7 col-12">
				<div class="row justify-content-center">
					<div class="col-12">
						<h1 class="block-title">
							<?php the_title(); ?>
						</h1>
					</div>
					<div class="col-12">
						<div class="base-output text-center mb-4">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<?php get_template_part('views/partials/repeat', 'sidebar'); ?>
			<div class="col-lg-9 col-md-7 col-12">
				<?php if ($posts->have_posts()) : ?>
					<div class="row align-items-stretch put-here-posts justify-content-center put-here-posts">
						<?php foreach ($posts->posts as $post) {
							get_template_part('views/partials/card', 'product_col',
									[
											'post' => $post,
									]);
						} ?>
					</div>
				<?php endif;
				if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 6)) : ?>
					<div class="row justify-content-center">
						<div class="col-auto">
							<div class="more-link base-link load-more-posts w-auto" data-type="product" data-count="<?= $num; ?>">
								טען עוד..
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>

