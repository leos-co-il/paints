<?php if (isset($args['links']) && $args['links']) : ?>
	<div class="row align-items-stretch justify-content-center">
		<?php foreach ($args['links'] as $link) : ?>
			<div class="col-auto repeat-link-col">
				<a href="<?= get_term_link($link); ?>" class="top-link-item">
					<?= $link->name; ?>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
