<?php
$terms = get_terms([
	'taxonomy'      => 'product_cat',
	'hide_empty'    => false,
	'parent' => 0,
]);
?>
<?php if ($terms) : ?>
	<div class="col-lg-3 col-md-5 col-12 position-relative mb-5">
		<div class="sidebar-wrapper sticky-sidebar">
			<ul class="sidebar-list">
				<?php foreach ($terms as $parent) :
					$terms_ch = get_terms( 'product_cat', array( 'parent' => $parent->term_id, 'orderby' => 'slug', 'hide_empty' => false ) ); ?>
					<li class="sidebar-item">
									<span class="sidebar-item-inside">
										<?php if ($terms_ch) : ?>
											<span class="sidebar-item-trigger"></span>
										<?php endif; ?>
										<a href="<?= get_term_link($parent); ?>" class="sidebar-item-main">
											<?= $parent->name; ?>
										</a>
									</span>
						<?php if ($terms_ch) : ?>
							<ul class="children-terms-wrap">
								<?php foreach ( $terms_ch as $term ) : ?>
									<li>
										<a href="<?= get_term_link( $term ); ?>"><?= $term->name; ?></a>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
<?php endif; ?>
