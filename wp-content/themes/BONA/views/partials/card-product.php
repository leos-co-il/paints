<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="post-card product-card">
		<a class="product-card-image" href="<?= $link; ?>">
			<?php if (has_post_thumbnail($args['post'])) : ?>
				<img src="<?= postThumb($args['post']); ?>" alt="post-image">
			<?php endif;?>
		</a>
		<div class="post-card-content">
			<div class="card-content-wrapper">
				<a class="post-card-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
				<p class="card-text">
					<?= text_preview($args['post']->post_content, 20); ?>
				</p>
			</div>
			<a href="<?= $link; ?>" class="post-card-link">
				פרטים נוספים
			</a>
		</div>
	</div>
<?php endif; ?>
