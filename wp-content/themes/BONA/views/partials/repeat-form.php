<section class="repeat-form-block">
	<div class="container-fluid">
		<div class="row justify-content-center form-wrapper">
			<div class="col-xl-8 col-lg-10 col-12">
				<div class="row align-items-center justify-content-center">
					<?php if ($title = opt('base_form_title')) : ?>
						<div class="col-auto p-0">
							<h2 class="form-title"><?= $title; ?></h2>
						</div>
					<?php endif;
					if ($subtitle = opt('base_form_subtitle')) : ?>
						<div class="col-auto">
							<h3 class="form-subtitle"><?= $subtitle; ?></h3>
						</div>
					<?php endif; ?>
				</div>
				<?php getForm('102'); ?>
			</div>
		</div>
	</div>
</section>
