<?php if(isset($args['posts']) && $args['posts']) : ?>
	<section class="slider-posts-block home-posts arrows-slider post-slider-arrows pt-0">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="base-title">
						<?= $args['title']; ?>
					</h2>
				</div>
			</div>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<div class="col-12">
					<div class="posts-slider" dir="rtl">
						<?php foreach ($args['posts'] as $post) : ?>
							<div>
								<?php get_template_part('views/partials/card', 'product',
									[
										'post' => $post,
									]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
