<?php if ( function_exists('yoast_breadcrumb') ) : ?>
	<div class="container-fluid">
		<div class="row justify-content-center bread-row">
			<div class="col-12">
				<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
			</div>
		</div>
	</div>
<?php endif; ?>
