<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xl-3 col-sm-6 col-12 col-post">
		<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-card-image" href="<?= $link; ?>"
				<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif;?>>
			</a>
			<div class="post-card-content">
				<div class="card-content-wrapper">
					<a class="post-card-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
					<p class="card-text">
						<?= text_preview($args['post']->post_content, 20); ?>
					</p>
				</div>
				<a href="<?= $link; ?>" class="post-card-link">
					קרא עוד
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
