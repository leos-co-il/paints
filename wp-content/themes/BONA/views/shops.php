<?php
/*
Template Name: חנויות
*/

get_header();
$fields = get_fields();
$areas = get_terms([
	'taxonomy'      => 'area',
	'hide_empty'    => false,
	'parent' => 0,
]);
$_area_type = (isset($_GET['shop-area'])) ? $_GET['shop-area'] : null;
$_city_type = (isset($_GET['shop-city'])) ? $_GET['shop-city'] : null;
$query_args = [
		'posts_per_page' => -1,
		'post_type' => 'shop',
];
$query_args['tax_query'] = [
		'relation' => 'AND',
		$_area_type ? [
				'taxonomy' => 'area',
				'field'    => 'term_id',
				'terms'    => [$_area_type],
		] : null,
		$_city_type ? [
				'taxonomy' => 'area',
				'field'    => 'term_id',
				'terms'    => [$_city_type],
		] : null,
	];
$shops = new WP_Query($query_args);
$points = [];
?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h1 class="block-title">
					<?php the_title(); ?>
				</h1>
				<div class="base-output text-center shops-page-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-auto">
				<form class="form-terms-product form-group">
					<?php if ($areas) : ?>
						<select name="shop-area" class="select-product" id="inputArea">
							<option disabled>בחר אזור בארץ</option>
							<?php foreach ($areas as $area) : ?>
								<option value="<?= $area->term_id; ?>" data-id="<?= $area->term_id; ?>">
									<?= $area->name; ?>
								</option>
							<?php endforeach; ?>
						</select>
					<?php endif; ?>
					<select name="shop-city" class="select-product" id="inputCity">
						<option>בחר עיר</option>
					</select>
				</form>
			</div>
		</div>
		<div class="row pt-3 pb-5 row-projects">
			<div class="col-xl-7 col-12 col-projects-out mb-xl-0 mb-4 ">
				<div class="markers-list rtl-back">
					<div class="row align-items-stretch row-border-bottom">
						<div class="col-4">
							<h3 class="table-title">שם חנות</h3>
						</div>
						<div class="col-4">
							<h3 class="table-title">עיר</h3>
						</div>
						<div class="col-4">
							<h3 class="table-title">צרו קשר</h3>
						</div>
					</div>
					<?php foreach($shops->posts as $mrk => $project):
						$point = get_field('loc', $project); $points[] = $point;?>
						<div class="row marker marker-item marker-id-<?= $mrk ?>" data-id ="<?= $mrk ?>"
							 data-lat="<?php echo esc_attr($point['lat']); ?>"
							 data-lng="<?php echo esc_attr($point['lng']); ?>">
							<div class="col-4 bl-light">
								<span class="base-text">
									<?= $project->post_title; ?>
								</span>
							</div>
							<div class="col-4 bl-light">
								<span class="base-text">
									<?php $post_terms = wp_get_object_terms($project->ID, 'area', ['fields' => 'all']);
									foreach ($post_terms as $term) {
										if ($term->parent !== 0) {
											echo $term->name;
										}
									}?>
								</span>
							</div>
							<div class="col-4 trigger-shop-col">
								<span class="base-text">
									פרטי התקשרות
								</span>
								<span class="trigger-shop-item"></span>
							</div>
							<div class="col-12 marker-addition">
								<div class="row justify-content-between">
									<div class="col-lg-4 col-md-6 col-8">
										<div class="marker-content">
											<h4 class="base-text font-weight-normal">
												<?= $project->post_title; ?>
											</h4>
											<p class="base-text">
												<?= get_field('shop_addr', $project->ID); ?>
											</p>
											<?php if ($tel = get_field('phone', $project->ID)) : ?>
												<a class="base-text" href="tel:<?= $tel; ?>">
													<?= $tel; ?>
												</a>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-4">
										<?php if ($url = get_field('site_url', $project->ID)) : ?>
											<a class="base-text" href="<?= $url; ?>">
												מעבר לאתר החנות >
											</a>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="col-xl-5 col-12 rtl-back">
				<div class="acf-sales-map" data-zoom="16">
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
