<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
$post_gallery = $fields['post_gallery'];
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container-fluid">
		<div class="row justify-content-between">
			<div class="<?= ($post_gallery || has_post_thumbnail()) ? 'col-xl-5 col-lg-6 col-12' : 'col-12'; ?>">
				<h1 class="block-title text-right"><?php the_title(); ?></h1>
				<div class="base-output post-output">
					<?php the_content(); ?>
				</div>
				<div class="socials-share">
					<span class="share-text">
						 שתף
					</span>
					<!--	WHATSAPP-->
					<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.4s">
						<img src="<?= ICONS ?>whatsapp-share.png">
					</a>
					<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.2s">
						<img src="<?= ICONS ?>facebook-share.png">
					</a>
					<!--	MAIL-->
					<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.6s">
						<img src="<?= ICONS ?>mail-share.png">
					</a>
				</div>
			</div>
			<?php if ($post_gallery || has_post_thumbnail()) : ?>
				<div class="col-lg-6 col-12 arrows-slider arrows-slider-gallery">
					<div class="arrows-slider post-gallery-block">
						<div class="gallery-slider" dir="rtl">
							<?php if(has_post_thumbnail()): ?>
								<div class="p-1">
									<a class="big-slider-item" style="background-image: url('<?= postThumb(); ?>')"
									   href="<?= postThumb(); ?>" data-lightbox="images"></a>
								</div>
							<?php endif;
							if ($post_gallery) : foreach ($post_gallery as $img): ?>
								<div class="p-1">
									<a class="big-slider-item" style="background-image: url('<?= $img['url']; ?>')"
									   href="<?= $img['url']; ?>" data-lightbox="images">
									</a>
								</div>
							<?php endforeach; endif; ?>
						</div>
						<?php if ($post_gallery) : ?>
							<div class="thumbs-wrap">
								<div class="thumbs" dir="rtl">
									<?php if(has_post_thumbnail()): ?>
										<div class="p-1">
											<a class="thumb-item" style="background-image: url('<?= postThumb(); ?>')"
											   href="<?= postThumb(); ?>" data-lightbox="images-small"></a>
										</div>
									<?php endif; foreach ($post_gallery as $img): ?>
										<div class="p-1">
											<a class="thumb-item" style="background-image: url('<?= $img['url']; ?>')"
											   href="<?= $img['url']; ?>" data-lightbox="images-small">
											</a>
										</div>
									<?php endforeach; ?>
								</div>
								<div class="put-arrows-here"></div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$type = get_post_type();
switch ($type) {
	case 'project':
		$same_title = $fields['same_projects_title'] ? $fields['same_projects_title'] : 'לפרויקטים נוספים';
		$samePostsKey =  'same_projects';
		$term_type = 'project_cat';
		break;
	default:
		$same_title = $fields['same_posts_title'] ? $fields['same_posts_title'] : 'מאמרים נוספים';
		$samePostsKey = 'same_posts';
		$term_type = 'category';
		break;
}
$post_terms = wp_get_object_terms($postId, $term_type, ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'numberposts' => 5,
	'post_type' => $type,
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => $term_type,
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields[$samePostsKey]) {
	$samePosts = $fields[$samePostsKey];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'numberposts' => 5,
		'orderby' => 'rand',
		'post_type' => $type,
		'post__not_in' => array($postId),
	]);
}
$sameProducts = isset($fields['same_products']) && $fields['same_products'] ? $fields['same_products'] : get_posts([
	'posts_per_page' => 5,
	'orderby' => 'rand',
	'post_type' => 'product',
]);
if ($sameProducts) {
	get_template_part('views/partials/slider', 'products', [
		'title' => $fields['same_prod_title'] ? $fields['same_prod_title'] : 'המוצרים בהם נעשה שימוש בפרויקט',
		'posts' => $sameProducts,
	]);
}
if ($samePosts) {
	get_template_part('views/partials/slider', 'posts', [
		'title' => $same_title,
		'posts' => $samePosts,
	]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
