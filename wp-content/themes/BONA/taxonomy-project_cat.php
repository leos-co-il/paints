<?php
get_header();
$query = get_queried_object();
$posts = new WP_Query([
	'posts_per_page' => 8,
	'post_type' => 'project',
	'suppress_filters' => false,
	'tax_query' => array(
		array(
			'taxonomy' => 'project_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'project',
	'suppress_filters' => false,
	'tax_query' => array(
		array(
			'taxonomy' => 'project_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
$terms = get_terms([
		'taxonomy'      => 'project_cat',
		'hide_empty'    => false,
		'parent' => 0,
]);
?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="block-title">
					<?= $query->name; ?>
				</h1>
			</div>
		</div>
		<?php if ($terms) {
			get_template_part('views/partials/repeat', 'links',
					[
							'links' => $terms,
					]);
		} ?>
		<div class="row">
			<div class="col-12">
				<div class="base-output text-center">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post_col',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 8)) : ?>
			<div class="row justify-content-center mt-4">
				<div class="col-auto">
					<div class="more-link base-link load-more-posts" data-type="project" data-count="<?= $num; ?>"
						 data-term="<?= $query->term_id; ?>" data-term_name="project_cat">
						טען עוד..
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $slider,
		'img' => get_field('slider_img', $query),
	]);
}
get_footer(); ?>

