<?php
if(CATALOG){
    function product_post_type() {

        $labels = array(
            'name'                => 'מוצרים',
            'singular_name'       => 'מוצרים',
            'menu_name'           => 'מוצרים',
            'parent_item_colon'   => 'פריט אב:',
            'all_items'           => 'כל המוצרים',
            'view_item'           => 'הצג מוצר',
            'add_new_item'        => 'הוסף מוצר חדש',
            'add_new'             => 'הוסף חדש',
            'edit_item'           => 'ערוך מוצר',
            'update_item'         => 'עדכון מוצר',
            'search_items'        => 'חפש מוצר',
            'not_found'           => 'לא נמצא',
            'not_found_in_trash'  => 'לא מצא באשפה',
        );
        $rewrite = array(
            'slug'                => 'product',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => 'product',
            'description'         => 'מוצרים',
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
            'taxonomies'          => array( 'product_cat', 'product_size', 'product_color' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-products',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'product', $args );

    }

    add_action( 'init', 'product_post_type', 0 );

	function product_taxonomy() {

		$labels = array(
			'name'                       => 'קטגוריות מוצרים',
			'singular_name'              => 'קטגוריות מוצרים',
			'menu_name'                  => 'קטגוריות מוצרים',
			'all_items'                  => 'כל הקטגוריות',
			'parent_item'                => 'קטגורית הורה',
			'parent_item_colon'          => 'קטגורית הורה:',
			'new_item_name'              => 'שם קטגוריה חדשה',
			'add_new_item'               => 'להוסיף קטגוריה חדשה',
			'edit_item'                  => 'ערוך קטגוריה',
			'update_item'                => 'עדכן קטגוריה',
			'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
			'search_items'               => 'חיפוש קטגוריות',
			'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
			'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'product_cat',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'product_cat', array( 'product' ), $args );

	}

	add_action( 'init', 'product_taxonomy', 0 );

    function product_color_taxonomy() {

        $labels = array(
            'name'                       => 'צבעים',
            'singular_name'              => 'צבע',
            'menu_name'                  => 'צבעים',
            'all_items'                  => 'כל הצבעים',
            'parent_item'                => 'צבע הורה',
            'parent_item_colon'          => 'צבע הורה:',
            'new_item_name'              => 'שם צבע חדש',
            'add_new_item'               => 'להוסיף צבע חדש',
            'edit_item'                  => 'ערוך צבע',
            'update_item'                => 'עדכן צבע',
            'separate_items_with_commas' => 'צבעים נפרדות עם פסיק',
            'search_items'               => 'חיפוש צבעים',
            'add_or_remove_items'        => 'להוסיף או להסיר צבעים',
            'choose_from_most_used'      => 'בחר מהצבעים הנפוצות ביותר',
            'not_found'                  => 'לא נמצא',
        );
        $rewrite = array(
            'slug'                       => 'product_color',
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'product_color', array( 'product' ), $args );

    }

    add_action( 'init', 'product_color_taxonomy', 0 );

	function product_size_taxonomy() {

		$labels = array(
			'name'                       => 'גדלים',
			'singular_name'              => 'גודל',
			'menu_name'                  => 'גדלים',
			'all_items'                  => 'כל הגדלים',
			'parent_item'                => 'גודל הורה',
			'parent_item_colon'          => 'קטגורית הורה:',
			'new_item_name'              => 'שם גודל חדש',
			'add_new_item'               => 'להוסיף גודל חדש',
			'edit_item'                  => 'ערוך גודל',
			'update_item'                => 'עדכן גודל',
			'separate_items_with_commas' => 'גדלים נפרדות עם פסיק',
			'search_items'               => 'חיפוש גדלים',
			'add_or_remove_items'        => 'להוסיף או להסיר גדלים',
			'choose_from_most_used'      => 'בחר מהגדלים הנפוצות ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'product_size',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'product_size', array( 'product' ), $args );

	}

	add_action( 'init', 'product_size_taxonomy', 0 );

}

if(PROJECTS){
    function project_post_type() {

        $labels = array(
            'name'                => 'פרויקטים',
            'singular_name'       => 'פרויקטים',
            'menu_name'           => 'פרויקטים',
            'parent_item_colon'   => 'פריט אב:',
            'all_items'           => 'כל הפרויקטים',
            'view_item'           => 'הצג פרויקט',
            'add_new_item'        => 'הוסף פרויקט',
            'add_new'             => 'הוסף פרויקט חדש',
            'edit_item'           => 'ערוך פרויקט',
            'update_item'         => 'עדכון פרויקט',
            'search_items'        => 'חפש פריטים',
            'not_found'           => 'לא נמצא',
            'not_found_in_trash'  => 'לא מצא באשפה',
        );
        $rewrite = array(
            'slug'                => 'project',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => 'project',
            'description'         => 'פרויקטים',
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
            'taxonomies'          => array( 'project_cat' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-portfolio',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'project', $args );

    }

    add_action( 'init', 'project_post_type', 0 );

    function project_taxonomy() {

        $labels = array(
            'name'                       => 'קטגוריות פרויקטים',
            'singular_name'              => 'קטגוריות פרויקטים',
            'menu_name'                  => 'קטגוריות פרויקטים',
            'all_items'                  => 'כל הקטגוריות',
            'parent_item'                => 'קטגורית הורה',
            'parent_item_colon'          => 'קטגורית הורה:',
            'new_item_name'              => 'שם קטגוריה חדשה',
            'add_new_item'               => 'להוסיף קטגוריה חדשה',
            'edit_item'                  => 'ערוך קטגוריה',
            'update_item'                => 'עדכן קטגוריה',
            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
            'search_items'               => 'חיפוש קטגוריות',
            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
            'not_found'                  => 'לא נמצא',
        );
        $rewrite = array(
            'slug'                       => 'project_cat',
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'project_cat', array( 'project' ), $args );

    }

    add_action( 'init', 'project_taxonomy', 0 );
}

if(SHOPS){
	function shop_post_type() {

		$labels = array(
			'name'                => 'חנויות',
			'singular_name'       => 'חנות',
			'menu_name'           => 'חנויות',
			'parent_item_colon'   => 'פריט אב:',
			'all_items'           => 'כל החנויות',
			'view_item'           => 'הצג חנות',
			'add_new_item'        => 'הוסף חנות',
			'add_new'             => 'הוסף חנות חדשה',
			'edit_item'           => 'ערוך חנות',
			'update_item'         => 'עדכון חנות',
			'search_items'        => 'חפש פריטים',
			'not_found'           => 'לא נמצא',
			'not_found_in_trash'  => 'לא מצא באשפה',
		);
		$rewrite = array(
			'slug'                => 'shop',
			'with_front'          => true,
			'pages'               => true,
			'feeds'               => true,
		);
		$args = array(
			'label'               => 'shop',
			'description'         => 'חנויות',
			'labels'              => $labels,
			'supports'            => array( 'title', 'author'),
			'taxonomies'          => array( 'area', 'city' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-admin-multisite',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'     => 'post',
		);
		register_post_type( 'shop', $args );

	}

	add_action( 'init', 'shop_post_type', 0 );

	function area_taxonomy() {

		$labels = array(
			'name'                       => 'אזורים',
			'singular_name'              => 'אזור',
			'menu_name'                  => 'אזורים',
			'all_items'                  => 'כל האזורים',
			'parent_item'                => 'אזור הורה',
			'parent_item_colon'          => 'אזור הורה:',
			'new_item_name'              => 'שם אזור חדש',
			'add_new_item'               => 'להוסיף אזור חדש',
			'edit_item'                  => 'ערוך אזור',
			'update_item'                => 'עדכן אזור',
			'separate_items_with_commas' => 'אזורים נפרדות עם פסיק',
			'search_items'               => 'חיפוש אזורים',
			'add_or_remove_items'        => 'להוסיף או להסיר אזורים',
			'choose_from_most_used'      => 'בחר מהאזורים הנפוצים ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'area',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'area', array( 'shop' ), $args );

	}

	add_action( 'init', 'area_taxonomy', 0 );
}
