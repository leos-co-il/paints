<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
$post_gallery = $fields['post_gallery'];
$postId = get_the_ID();
$colors = wp_get_object_terms($postId, 'product_color', ['fields' => 'all']);
$sizes = wp_get_object_terms($postId, 'product_size', ['fields' => 'all']);
$accordion_info = [
	['title' => 'מידע טכני', 'content' => $fields['prod_info']],
	['title' => 'הוראות שימוש', 'content' => $fields['prod_rules_info']],
	['title' => 'קבצים להורדה' , 'content' => $fields['prod_files'], 'links' => true],
];
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h1 class="block-title block-title-mob text-right"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row justify-content-between">
			<div class="<?= ($post_gallery || has_post_thumbnail()) ? 'col-lg-6 col-12' : 'col-12'; ?>">
				<h1 class="block-title block-title-desc text-right"><?php the_title(); ?></h1>
				<?php if ($fields['articul']) : ?>
					<p class="base-text mb-2"><?= 'מק”ט: '.$fields['articul']; ?></p>
				<?php endif; ?>
				<div class="base-output post-output">
					<?php the_content(); ?>
				</div>
				<?php if ($colors || $sizes) : ?>
					<div class="form-terms-product">
						<?php if ($sizes) : ?>
							<select name="product-size" class="select-product">
								<option disabled>גודל</option>
								<?php foreach ($sizes as $size) : ?>
									<option value="<?= $size->name; ?>">
										<?= $size->name; ?>
									</option>
								<?php endforeach; ?>
							</select>
						<?php endif;
						if ($colors) : ?>
							<select name="product-size" class="select-product">
								<option disabled>גוון</option>
								<?php foreach ($colors as $color) : ?>
									<option value="<?= $color->name; ?>">
										<?= $color->name; ?>
									</option>
								<?php endforeach; ?>
							</select>
						<?php endif; ?>
					</div>
				<?php endif;
				if ($accordion_info) : ?>
					<div id="accordion-product" class="product-info-acc">
						<?php foreach ($accordion_info as $number => $info_item) : if ($info_item['content']) : ?>
							<div class="card">
								<div class="prod-desc-header" id="heading_<?= $number; ?>">
									<div class="accordion-button-wrap">
										<button class="btn" data-toggle="collapse"
												data-target="#contactInfo<?= $number; ?>"
												aria-expanded="false" aria-controls="collapseOne">
															<span class="accordion-symbol plus-icon
															<?php echo $number === 0 ? 'hide-icon' : ''; ?>">+</span>
											<span class="accordion-symbol minus-icon
															<?php echo $number === 0 ? 'show-icon' : ''; ?>">-</span>
										</button>
										<h2 class="product-info-title"><?= $info_item['title']; ?></h2>
									</div>
									<div id="contactInfo<?= $number; ?>" class="collapse
											<?php echo $number === 0 ? 'show' : ''; ?>"
										 aria-labelledby="heading_<?= $number; ?>" data-parent="#accordion-product">
										<?php if (!isset($info_item['links'])) : ?>
											<div class="base-output">
												<?= $info_item['content']; ?>
											</div>
										<?php else: if ($info_item['content']) : ?>
											<div class="d-flex justify-content-start flex-column">
												<?php foreach ($info_item['content'] as $item) : if ($item['file']) : ?>
													<a href="<?= $item['file']['url']; ?>" class="mr-4 mb-2 base-text">
														<?= $item['file']['title']; ?>
													</a>
												<?php endif; endforeach; ?>
											</div>
										<?php endif; endif; ?>
									</div>
								</div>
							</div>
						<?php endif; endforeach; ?>
					</div>
				<?php endif; ?>
				<div class="socials-share mt-3">
					<span class="share-text">
						 שתף
					</span>
					<!--	WHATSAPP-->
					<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.4s">
						<img src="<?= ICONS ?>whatsapp-share.png">
					</a>
					<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.2s">
						<img src="<?= ICONS ?>facebook-share.png">
					</a>
					<!--	MAIL-->
					<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.6s">
						<img src="<?= ICONS ?>mail-share.png">
					</a>
				</div>
				<div class="product-form-wrap">
					<h3 class="form-title">מעוניין במוצר זה?</h3>
					<?php getForm('101'); ?>
				</div>
			</div>
			<?php if ($post_gallery || has_post_thumbnail()) : ?>
				<div class="col-xl-5 col-lg-6 col-12 product-gallery-col arrows-slider arrows-slider-gallery">
					<div class="product-slider mb-3" dir="rtl">
						<?php if(has_post_thumbnail()): ?>
							<div class="p-1">
								<a class="big-slider-item-pro" href="<?= postThumb(); ?>" data-lightbox="images">
									<img src="<?= postThumb(); ?>" alt="product-image">
								</a>
							</div>
						<?php endif;
						if ($post_gallery) : foreach ($post_gallery as $img): ?>
							<div class="p-1">
								<a class="big-slider-item-pro" href="<?= $img['url']; ?>" data-lightbox="images">
									<img src="<?= $img['url']; ?>" alt="product-image">
								</a>
							</div>
						<?php endforeach; endif; ?>
					</div>
					<?php if ($post_gallery) : ?>
						<div class="thumbs-wrap">
							<div class="product-thumbs" dir="rtl">
								<?php if(has_post_thumbnail()): ?>
									<div class="p-1">
										<a class="thumb-item-pro" href="<?= postThumb(); ?>" data-lightbox="images-small">
											<img src="<?= postThumb(); ?>" alt="product-image">
										</a>
									</div>
								<?php endif; foreach ($post_gallery as $img): ?>
									<div class="p-1">
										<a class="thumb-item-pro" href="<?= $img['url']; ?>" data-lightbox="images-small">
											<img src="<?= $img['url']; ?>" alt="product-image">
										</a>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php
$samePosts = [];
$post_terms = wp_get_object_terms($postId, 'product_cat', ['fields' => 'ids']);
$samePosts = get_posts([
	'numberposts' => 5,
	'post_type' => 'product',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'product_cat',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_products']) {
	$samePosts = $fields['same_products'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'numberposts' => 5,
		'orderby' => 'rand',
		'post_type' => 'product',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) {
	get_template_part('views/partials/slider', 'products', [
		'title' => $fields['same_prod_title'] ? $fields['same_prod_title'] : 'מוצרים נוספים',
		'posts' => $samePosts,
	]);
}
get_footer(); ?>
