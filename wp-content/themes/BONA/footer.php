<?php

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$current_id = get_queried_object_id();
$contact_id = getPageByTemplate('views/contact.php');
?>

<footer>
	<div class="footer-main">
		<a id="go-top">
			<?= svg_simple(ICONS.'to-top.svg'); ?>
			<span class="base-text text-center">
				חזרה למעלה
			</span>
		</a>
		<?php if ($current_id !== $contact_id) : ?>
			<div class="footer-form-wrap">
				<div class="container-fluid">
					<div class="row justify-content-center">
						<div class="col-xl-8 col-lg-10 col-12">
							<div class="row justify-content-center">
								<div class="col-12">
									<div class="row align-items-center justify-content-center">
										<?php if ($title = opt('foo_form_title')) : ?>
											<div class="col-auto p-0">
												<h2 class="form-title"><?= $title; ?></h2>
											</div>
										<?php endif;
										if ($subtitle = opt('foo_form_subtitle')) : ?>
											<div class="col-auto">
												<h3 class="form-subtitle"><?= $subtitle; ?></h3>
											</div>
										<?php endif; ?>
									</div>
									<div class="middle-form wow zoomIn">
										<?php getForm('100'); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="container-fluid footer-container-menu">
			<div class="row justify-content-center">
				<div class="col-xl-8 col-lg-10 col-12">
					<div class="row justify-content-between align-items-start">
						<div class="col-xl-3 col-lg-auto col-sm-6 col-12 foo-menu main-foo-menu">
							<h3 class="foo-title">
								מפת האתר
							</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-menu', '1'); ?>
							</div>
						</div>
						<div class="col-lg col-12 foo-menu foo-links-menu">
							<?php ($foo_l_title = opt('foo_menu_title')); ?>
							<h3 class="foo-title">
								<?php echo $foo_l_title ? $foo_l_title : 'מאמרים רלוונטים'; ?>
							</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-links-menu', '1', 'hop-hey three-columns'); ?>
							</div>
						</div>
						<div class="col-xl-3 col-lg col-sm-6 foo-menu contacts-footer-menu">
							<h3 class="foo-title">
								צרו קשר
							</h3>
							<div class="menu-border-top">
								<ul class="contact-list d-flex flex-column">
									<?php if ($tel) : ?>
										<li>
											<a href="tel:<?= $tel; ?>" class="contact-info-footer">
												<?= 'נייד: '.$tel; ?>
											</a>
										</li>
									<?php endif; ?>
									<?php if ($address) : ?>
										<li>
											<a href="https://waze.com/ul?q=<?= $address; ?>"
											   class="contact-info-footer" target="_blank">
												<?= 'כתובת ראשית: '.$address; ?>
											</a>
										</li>
									<?php endif; ?>
									<?php if ($mail) : ?>
										<li>
											<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
												<?= 'מייל לפניות: '.$mail; ?>
											</a>
										</li>
									<?php endif; ?>
								</ul>
								<div class="foo-socials">
									<?php if ($whatsapp = opt('whatsapp')) : ?>
										<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="social-link-item" target="_blank">
											<img src="<?= ICONS ?>whatsapp.png" alt="whatsapp">
										</a>
									<?php endif;
									if ($facebook = opt('facebook')) : ?>
										<a href="<?= $facebook; ?>" class="social-link-item" target="_blank">
											<img src="<?= ICONS ?>facebook.png" alt="facebook">
										</a>
									<?php endif;
									if ($youtube = opt('youtube')) : ?>
										<a href="<?= $youtube; ?>" class="social-link-item" target="_blank">
											<img src="<?= ICONS ?>youtube.png" alt="youtube">
										</a>
									<?php endif;
									if ($mail) : ?>
										<a href="mailto:<?= $mail; ?>" class="social-link-item" target="_blank">
											<img src="<?= ICONS ?>foo-mail.png" alt="email">
										</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>
<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
