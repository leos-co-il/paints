<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_head(); ?>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
	<script>
		const timerStart = Date.now();
	</script>
	<div class="debug bg-danger border">
		<p class="width">
			<span>Width:</span>
			<span class="val"></span>
		</p>
		<p class="height">
			<span>Height:</span>
			<span class="val"></span>
		</p>
		<p class="media-query">
			<span>Media Query:</span>
			<span class="val"></span>
		</p>
		<p class="zoom">
			<span>Zoom:</span>
			<span class="val"></span>
		</p>
		<p class="dom-ready">
			<span>DOM Ready:</span>
			<span class="val"></span>
		</p>
		<p class="load-time">
			<span>Loading Time:</span>
			<span class="val"></span>
		</p>
	</div>
<?php endif; ?>
<?php $links = opt('home_links'); ?>
<header class="header-pages sticky">
	<div class="header-versions">
		<?php foreach ($links as $link_current) : ?>
			<div class="header-version">
				<a href="<?= ($link_current['h_link'] && isset($link_current['h_link']['url'])) ? $link_current['h_link']['url']
						: ''; ?>" class="base-link-header setwebcookie" data-web="<?= ($link_current['h_page_title'] == 'Professional') ? 'pro' : 'home'; ?>">
					<?= $link_current['h_page_title']; ?>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-lg-3 col-md-2 col d-flex justify-content-start">
			</div>
			<div class="col-md col-auto">
				<nav id="MainNav" class="h-100">
					<div id="MobNavBtn">
						<span></span>
						<span></span>
						<span></span>
					</div>
					<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
				</nav>
			</div>
			<div class="col-lg-3 col-md-2 col d-flex justify-content-end align-items-center">
				<div class="search-wrapper">
					<div class="search-trigger">
						<img src="<?= ICONS ?>search.svg" alt="search">
					</div>
					<div class="float-search">
						<?php get_search_form(); ?>
					</div>
				</div>
				<?php if ($tel = opt('tel')) : ?>
					<a href="tel:<?= $tel; ?>" class="header-tel">
						<span class="tel-number"><?= $tel; ?></span>
						<img src="<?= ICONS ?>form-tel.png" alt="phone">
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</header>
