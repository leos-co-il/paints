<?php

the_post();
get_header();
$fields = get_fields();
$query = get_queried_object();
$posts = new WP_Query([
	'posts_per_page' => 6,
	'post_type' => 'product',
	'tax_query' => array(
		array(
			'taxonomy' => 'product_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
$published_posts = new WP_Query([
    'posts_per_page' => -1,
    'post_type' => 'product',
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id',
            'terms' => $query->term_id,
        )
    )
]);
?>
<article class="page-body">
	<div class="container-fluid">
		<div class="row justify-content-end">
			<div class="col-lg-9 col-md-7 col-12">
				<div class="row justify-content-center">
					<div class="col-12">
						<h1 class="block-title">
							<?= $query->name; ?>
						</h1>
					</div>
					<div class="col-12">
						<div class="base-output text-center mb-4">
							<?= category_description(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<?php get_template_part('views/partials/repeat', 'sidebar'); ?>
			<div class="col-lg-9 col-md-7 col-12">
				<?php if ($posts->have_posts()) : ?>
					<div class="row align-items-stretch put-here-posts justify-content-center put-here-posts">
						<?php foreach ($posts->posts as $post) {
							get_template_part('views/partials/card', 'product_col',
								[
									'post' => $post,
								]);
						} ?>
					</div>
				<?php endif;
				if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 8)) : ?>
					<div class="row justify-content-center mt-4">
						<div class="col-auto">
							<div class="more-link base-link load-more-posts" data-type="product" data-count="<?= $num; ?>"
								 data-term="<?= $query->term_id; ?>" data-term_name="product_cat">
								טען עוד..
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>

